# 数据操作

## Tasks

### 1. 新增数据
请向如下表里面新增如下数据：

##### 1. 学生表(student):

   |  id  | name | age  | gender  |  phone   |
   | :--: | :--: | :--: | :--: | :--: | 
   | 1  | 张三 |  18  |  男  | 15342349123 |
   | 2  | 李四 |  20  |  女  | 13884356789 |
   | 3  | 王五 |  26  |  女  | 13884359789 |
   | 4  | 赵六 |  16  |  女  | 13884334789 |

##### 2. 老师表(teacher):

   |  id  | name | age  | gender  |  phone   | subject_id |
   | :--: | :--: | :--: | :--: | :--: | :--: | 
   | 1  | 刘老师 |  38  |  男  | 15343349123 | 1 |
   | 2  | 李老师 |  40  |  女  | 13885356789 | 2 |
   | 3  | 张老师 |  56  |  女  | 18784359789 | 3 |



##### 3. 科目表(subject)：

   |  id  | subject | period |   description    |
   | :--: | :-----: | :-----: | :--------------: |
   | 1 |  语文   | 40  |  |
   | 2 |  数学   | 30  |  |
   | 3 |  英语   | 45  |  |

##### 4. 考试表
   |  id  |  name | subject_id |
   | :--: | :--------: | :--------: |
   |  1   |    期末考试     |    1    |
   |  2   |    期末考试     |    2    |
   |  3   |    中期考试     |    3    |
   |  4   |    中期考试     |    1    |

##### 5. 成绩表(score)：

   |  id  | student_id | exam_id | score |
   | :--: | :--------: | :--------: | :---: |
   |  1   |    1     |    1    |  80   |
   |  2   |    1     |    2    |  60   |
   |  3   |    1     |    3    |  70   |
   |  4   |    1     |    4    | 60.5  |
   |  5   |    2     |    1    |  40   |
   |  6   |    2     |    2    |  30   |
   |  7   |    2     |    3    |  20   |
   |  8   |    2     |    4    |  55   |
   |  9   |    3     |    1    |  48   |
   |  10  |    3     |    2    |  39   |
   |  11  |    3     |    3    |  70   |
   |  12  |    3     |    4    |  95   |
   |  13  |    4     |    1    |  78   |
   |  14  |    4     |    2    |  88   |
   |  15  |    4     |    3    |  0    |
   |  16  |    4     |    4    |  100   |

### 2. 修改数据
1. 修改学生赵六的名字的联系方式为：13456789876
2. 给语文这个科目添加描述：该课程是本校特殊课程，由非常资深的刘老师授课。
3. 修改赵六同学语文中期考试的成绩为98分

### 3. 查询数据
1. 查询所有学生。
2. 查询所有成年学生(年龄大于等于18岁)。

### 4. 删除数据
1. 由于赵六同学在语文中期考试中作弊，于是需要在成绩表中删除该记录。

## 你的答案：
请按照上面的Task，用SQL实现后，将实现后的效果和SQL写在下面，并提交。

### 1. 新增数据
INSERT INTO `学生表(student)` VALUES(3,'王五',26,'女','13884359789'),(4,'赵六',16,'女','13884334789');
INSERT INTO `老师表(teacher)` VALUES(1,'刘老师',38,'男','15342349123',1),(2,'李老师',40,'女','13885356789',2),(3,'张老师',56,'女','18784359789',3);
INSERT INTO `科目表(subject)` VALUES(1,'语文',40,''),(2,'数学',30,''),(3,'英语',45,'');
INSERT INTO `考试表(exam)` VALUES(1,'期末考试',1),(2,'期末考试',2),(3,'中期考试',3),(4,'中期考试',1);
INSERT INTO `成绩表(score)` VALUES(1,1,1,80),(2,1,2,60),(3,1,3,70),(4,1,4,60.5),(5,2,1,40),(6,2,2,30),(7,2,3,20),(8,2,4,55),(9,3,1,48),(10,3,2,39),(11,3,3,70),(12,3,4,95),(13,4,1,78),(14,4,2,88),(15,4,3,0),(16,4,4,100);
![](https://icon.qiantucdn.com/20200410/fdf79d76d7be835225bbd6d9216f07162)

### 2. 修改数据
UPDATE `学生表(student)` SET `联系方式(phone)`='13456789876' WHERE `学号(id)`=4;
![](https://icon.qiantucdn.com/20200410/6eb0f779565f6a06a56a32875520625f2)

UPDATE `科目表(subject)` SET `描述(description)` = '该课程是本校特殊课程，由非常资深的刘老师授课' WHERE `编号(id)`=1;
![](https://icon.qiantucdn.com/20200410/787d7001c21e7bb75853015f2d53fd992)


UPDATE 
	`成绩表(score)`
SET 
	`分数(score)` = 98 
WHERE `学生(student_id)`= (
	 SELECT `学号(id)` 
    FROM `学生表(student)` 
    WHERE `姓名(name)` = '赵六'
    )
	 AND `考试(exam_id)`=(
    SELECT `编号(id)` 
    FROM `考试表(exam)` 
    WHERE `考试名称(name)`='中期考试' 
	   AND `科目(subject_id)` = (
         SELECT `编号(id)` 
         FROM `科目表(subject)` 
         WHERE `科目名称(name)` = '语文'
         )
   );
![](https://icon.qiantucdn.com/20200410/3b6487aaf54dd1dca03a735e483d083a2)

### 3. 查询数据
SELECT 
	`学号(id)`,
	`姓名(name)`,
    `年龄(age)`,
    `性别(sex)`,
    `联系方式(phone)` 
FROM 
	student_manage_sys_yangkunru.`学生表(student)`;
![](https://icon.qiantucdn.com/20200410/6eb0f779565f6a06a56a32875520625f2)

SELECT 
	`学号(id)`,
	`姓名(name)`,
    `年龄(age)`,
    `性别(sex)`,
    `联系方式(phone)` 
FROM 
	student_manage_sys_yangkunru.`学生表(student)`
WHERE `年龄(age)`>=18;
![](https://icon.qiantucdn.com/20200410/98cfcea98897991e7522d6c3deaa19462)

### 4. 删除数据
DELETE FROM `成绩表(score)` 
WHERE `学生(student_id)`= (
	SELECT `学号(id)` 
    FROM `学生表(student)` 
    WHERE `姓名(name)` = '赵六'
    )
	AND `考试(exam_id)`=(
    SELECT `编号(id)` 
    FROM `考试表(exam)` 
    WHERE `考试名称(name)`='中期考试' 
	   AND `科目(subject_id)` = (
         SELECT `编号(id)` 
         FROM `科目表(subject)` 
         WHERE `科目名称(name)` = '语文'
         )
   );
![](https://icon.qiantucdn.com/20200410/907f2f8cdf307faf47cf9aff191f7a122)
   
